#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=C:\Program Files (x86)\AutoIt3\Icons\au3.ico
#AutoIt3Wrapper_Compile_Both=y
#AutoIt3Wrapper_Res_ProductName=indexnow
#AutoIt3Wrapper_Res_Language=1049
#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#AutoIt3Wrapper_Run_Au3Stripper=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         https://gitlab.com/intraum

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

#Region ; зависимости

;#include <Array.au3> ; для проверок массивов
#include <FileConstants.au3>
#include <GuiEdit.au3> ; для ctrl + a
#include <String.au3> ; для перегонки ключа и его пути в шестнадцатеричный формат и обратно
#include "zavisimosti/Json.au3"
#include "zavisimosti/StringAux.au3"
#include "zavisimosti/GUIAux.au3"

; -- ISN Form Studio -- ;
#include <StaticConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <GuiButton.au3>
#include <EditConstants.au3>
#include <ProgressConstants.au3>

#EndRegion ; зависимости


#Region ; настройки AutoIt

Opt("GUIOnEventMode", 1)

#EndRegion ; настройки AutoIt


#Region ; обычные константы

Global Const $eMsgBoxTitle = "IndexNow - отправка URL'ов"

#EndRegion ; обычные константы


#Region ; класс настроек

Global Const $eSettingsGUID = "1e192a0d-0c8b-427a-a8bd-d24bc2a969cd"

Global Enum _
		$eSett_sGUID, _            ; GUID
		$eSett_sPath, _            ; путь к файлу настроек
		$eSett_sSection, _         ; секция main
		$eSett_sKeyINKey, _        ; ключ настроек ключа IndexNow
		$eSett_sKeyINKeyURL, _     ; ключ настроек урла ключа IndexNow
		$eSett_sKeyYandex, _       ; ключ настроек для Яндекса
		$eSett_sKeyBing, _         ; ключ настроек для Bing'а
		$eSett_sKeySaveKey, _      ; ключ настроек сохранения ключа IndexNow и его урла
		$eSett_sINKey, _           ; ключ IndexNow
		$eSett_sINKeyURL, _        ; урл файла ключа
		$eSett_iSaveKey, _         ; сохранить ключ и урл ключа в настройки (иначе не сохранять)
		$eSett_iYandex, _          ; работать с Яндексом
		$eSett_iBing, _            ; работать с Бингом
		$eSett_iSize               ; размер массива-"объекта"


Func Settings() ; конструктор

	Local $hSettings[$eSett_iSize]

	$hSettings[$eSett_sGUID] = $eSettingsGUID

	Local $sFilePath = _
			@ScriptDir & '\' _
			 & StringTrimRight(@ScriptName, StringLen(@ScriptName) _
			 - StringInStr(@ScriptName, '.', Default, -1)) & 'ini'

	$hSettings[$eSett_sPath] = $sFilePath
	$hSettings[$eSett_sSection] = "main"
	$hSettings[$eSett_sKeyINKey] = "key"
	$hSettings[$eSett_sKeyINKeyURL] = "url"
	$hSettings[$eSett_sKeySaveKey] = "savekey"
	$hSettings[$eSett_sKeyYandex] = "yandex"
	$hSettings[$eSett_sKeyBing] = "bing"

	Return $hSettings

EndFunc   ;==>Settings


; здесь и далее "методы" с нижним подчёркиванием являются "частными" -- предназнечены к использованию только внутри "класса";
; здесь и далее - частные методы _IsObject для проверок, тот ли "объект"

Func _Settings_IsObject(ByRef Const $hSettings)

	Return UBound($hSettings) = $eSett_iSize And $hSettings[$eSett_sGUID] == $eSettingsGUID

EndFunc   ;==>_Settings_IsObject


; получение значения 1/0 и сохранение в "объект" по индексу массива-"объекта"

Func _Settings_SetOneZero(ByRef $hSettings, $i)

	Local $vKey

	Switch $i

		Case $eSett_iSaveKey

			$vKey = $eSett_sKeySaveKey

		Case $eSett_iYandex

			$vKey = $eSett_sKeyYandex

		Case $eSett_iBing

			$vKey = $eSett_sKeyBing

	EndSwitch

	$vKey = $hSettings[$vKey]

	Local $vValue = IniRead($hSettings[$eSett_sPath], $hSettings[$eSett_sSection], $vKey, '0')

	$vValue = (Int($vValue) = 1) ? 1 : 0

	$hSettings[$i] = $vValue

EndFunc   ;==>_Settings_SetOneZero


Func Settings_SetAll(ByRef $hSettings)

	If Not _Settings_IsObject($hSettings) Then Return

	If Not FileExists($hSettings[$eSett_sPath]) Then ; настройки по умолчанию

		$hSettings[$eSett_sINKey] = ''
		$hSettings[$eSett_sINKeyURL] = ''
		$hSettings[$eSett_iSaveKey] = 1
		$hSettings[$eSett_iYandex] = 1
		$hSettings[$eSett_iBing] = 0
		Return
	EndIf

	; ключ и путь сохранены в шестнадцатеричном формате

	$hSettings[$eSett_sINKey] = _HexToString(IniRead( _
			$hSettings[$eSett_sPath], _
			$hSettings[$eSett_sSection], _
			$hSettings[$eSett_sKeyINKey], _
			''))

	$hSettings[$eSett_sINKeyURL] = _HexToString(IniRead( _
			$hSettings[$eSett_sPath], _
			$hSettings[$eSett_sSection], _
			$hSettings[$eSett_sKeyINKeyURL], _
			''))

	_Settings_SetOneZero($hSettings, $eSett_iSaveKey)
	_Settings_SetOneZero($hSettings, $eSett_iYandex)
	_Settings_SetOneZero($hSettings, $eSett_iBing)

EndFunc   ;==>Settings_SetAll


; для записи одной настройки в файл по индексу массива-"объекта";
; 2й параметр -- ключ настроек, куда пишем значение (3й параметр);
; 4й параметр обеспечивает сохранение ключа и пути в "закодированном" виде;
; base64 не используется, т.к. ни одна UDF не устроила

Func Settings_WriteItem(ByRef Const $hSettings, $iKey, $vValue, $bToHex = False)

	If Not _Settings_IsObject($hSettings) Then Return

	Local $i ; индекс "объекта", в котором сохранено текущее значение настройки

	Switch $iKey

		Case $eSett_sKeyINKey

			$i = $eSett_sINKey

		Case $eSett_sKeyINKeyURL

			$i = $eSett_sINKeyURL

		Case $eSett_sKeySaveKey

			$i = $eSett_iSaveKey

		Case $eSett_sKeyYandex

			$i = $eSett_iYandex

		Case $eSett_sKeyBing

			$i = $eSett_iBing

	EndSwitch

	; перевод в строку для савнения одинаковых типов данных

	If String($hSettings[$i]) = String($vValue) Then Return ; не переписывать значение, если совпадает

	If $bToHex Then $vValue = _StringToHex($vValue)

	IniWrite( _
			$hSettings[$eSett_sPath], _
			$hSettings[$eSett_sSection], _
			$hSettings[$iKey], $vValue)

EndFunc   ;==>Settings_WriteItem


; для создания файла настроек

Func Settings_CreateFile(ByRef Const $hSettings)

	If Not _Settings_IsObject($hSettings) Then Return

	If FileExists($hSettings[$eSett_sPath]) Then Return SetError(1)

	Local $sFileContents = "[" & $hSettings[$eSett_sSection] & "]"
	$sFileContents &= @CRLF

	_Settings_AppendOneLine($hSettings, $eSett_sKeyINKey, $sFileContents)
	_Settings_AppendOneLine($hSettings, $eSett_sKeyINKeyURL, $sFileContents)
	_Settings_AppendOneLine($hSettings, $eSett_sKeySaveKey, $sFileContents)
	_Settings_AppendOneLine($hSettings, $eSett_sKeyYandex, $sFileContents)
	_Settings_AppendOneLine($hSettings, $eSett_sKeyBing, $sFileContents)

	; юникод нельзя для файла настроек, иначе криво работает с кириллицей

	Local $hFile = FileOpen($hSettings[$eSett_sPath], $FO_OVERWRITE + $FO_ANSI)

	FileWrite($hFile, $sFileContents)
	FileClose($hFile)

EndFunc   ;==>Settings_CreateFile


; для добавления строки при сборке файла настроек

Func _Settings_AppendOneLine(ByRef Const $hSettings, $i, ByRef $sFileContents)

	$sFileContents &= $hSettings[$i] & '=' & @CRLF

EndFunc   ;==>_Settings_AppendOneLine

#EndRegion ; класс настроек


#Region ; класс подготовки JSON

Global Const $ePJGUID = "b41e692d-1a31-4942-bcec-8b5936b7f66a"

Global Enum _
		$ePJ_sGUID, _          ; GUID
		$ePJ_aURLs, _          ; урлы для отправки
		$ePJ_sHost, _          ; домен сайта
		$ePJ_sProto, _         ; протокол (http:// или https://)
		$ePJ_sKey, _           ; ключ
		$ePJ_sKeyLoc, _        ; урл ключа
		$ePJ_iSize             ; размер массива-"объекта"


Func PrepareJSON() ; конструктор

	Local $hPJ[$ePJ_iSize]

	$hPJ[$ePJ_sGUID] = $ePJGUID

	Return $hPJ

EndFunc   ;==>PrepareJSON


Func _PrepareJSON_IsObject(ByRef Const $hPJ)

	Return UBound($hPJ) = $ePJ_iSize And $hPJ[$ePJ_sGUID] == $ePJGUID

EndFunc   ;==>_PrepareJSON_IsObject


Func _PrepareJSON_SetURLs(ByRef $hPJ, ByRef $aURLs)

	$hPJ[$ePJ_aURLs] = $aURLs
	$aURLs = 0

EndFunc   ;==>_PrepareJSON_SetURLs


; установка хоста и протокола; требуется наличие массива урлов

Func _PrepareJSON_SetHost(ByRef $hPJ)

	If Not IsArray($hPJ[$ePJ_aURLs]) Then Return

	; на основе первого урла

	$hPJ[$ePJ_sHost] = _StringGetRelURL(($hPJ[$ePJ_aURLs])[0], True)

	Local $sProto = "https://" ; выставляем протокол

	$hPJ[$ePJ_sProto] = _
			StringInStr(($hPJ[$ePJ_aURLs])[0], $sProto) _
			 ? $sProto _
			 : StringReplace($sProto, "s", '') ; получится http://

EndFunc   ;==>_PrepareJSON_SetHost


; исправить (задать) путь к ключу, если не был указан;
; зависимости: хост и протокол должны быть заданы

Func _PrepareJSON_FixKeyLoc(ByRef $hPJ)

	If $hPJ[$ePJ_sKeyLoc] Then Return ; не править указанный юзером урл

	$hPJ[$ePJ_sKeyLoc] = $hPJ[$ePJ_sProto] & $hPJ[$ePJ_sHost]
	$hPJ[$ePJ_sKeyLoc] &= '/' & $hPJ[$ePJ_sKey] & '.txt'

EndFunc   ;==>_PrepareJSON_FixKeyLoc


; конвертировать данные в JSON; зависимости:
; хост, ключ, путь к ключу, массив урлов

Func _PrepareJSON_SetJSON(ByRef $hPJ, ByRef $sJSON)

	Local $oJSON = Json_ObjCreate()

	Json_ObjPut($oJSON, "host", $hPJ[$ePJ_sHost])

	Json_ObjPut($oJSON, "key", $hPJ[$ePJ_sKey])

	Json_ObjPut($oJSON, "keyLocation", $hPJ[$ePJ_sKeyLoc])

	Json_ObjPut($oJSON, "urlList", $hPJ[$ePJ_aURLs])

	$sJSON = Json_Encode($oJSON, $JSON_UNESCAPED_SLASHES) ; + $JSON_PRETTY_PRINT для диагностики

EndFunc   ;==>_PrepareJSON_SetJSON


; 2й параметр - урлы, заданные юзером; 3й - ключ; 4й - урл ключа (как правило, пустое значение);
; 5й - куда сохраняем подготовленный JSON

Func PrepareJSON_Core(ByRef $hPJ, ByRef $aURLs, $sKey, $sKeyLoc, ByRef $sJSON)

	If Not _PrepareJSON_IsObject($hPJ) Then Return

	_PrepareJSON_SetURLs($hPJ, $aURLs)
	_PrepareJSON_SetHost($hPJ)

	$hPJ[$ePJ_sKey] = $sKey
	$hPJ[$ePJ_sKeyLoc] = $sKeyLoc

	_PrepareJSON_FixKeyLoc($hPJ)
	_PrepareJSON_SetJSON($hPJ, $sJSON)

EndFunc   ;==>PrepareJSON_Core

#EndRegion ; класс подготовки JSON


#Region ; класс отправки и получения данных

Global Const $eDEGUID = "2d80a07e-7183-48f0-b7db-0b5527266fb1"

Global Enum _
		$eDE_sGUID, _          ; GUID
		$eDE_sTextResponse, _  ; ответ сервера
		$eDE_iResponseCode, _  ; заголовки в ответе
		$eDE_vJSON, _          ; JSON в виде строки или в виде объекта
		$eDE_sPrettyPrint, _   ; вывод информации для юзера
		$eDE_sMessageSent, _   ; статус отправлено
		$eDE_sMessageError, _  ; статус ошибка
		$eDE_aEngines, _       ; массив, значениями которого являются базовые урлы поисковиков
		$eDE_iEngine, _        ; текущий поисковик (для сравнения со значением поисковика)
		$eDE_iYandex, _        ; не свойство - для задания в свойство текущего поисковика
		$eDE_iBing, _          ; не свойство - для задания в свойство текущего поисковика
		$eDE_sEngineBase, _    ; базовый урл поисковика
		$eDE_iSize             ; размер массива-"объекта"


Func DataExchange() ; конструктор

	Local $hDE[$eDE_iSize]

	$hDE[$eDE_sGUID] = $eDEGUID

	Return $hDE

EndFunc   ;==>DataExchange


Func _DataExchange_IsObject(ByRef Const $hDE)

	Return UBound($hDE) = $eDE_iSize And $hDE[$ePJ_sGUID] == $eDEGUID

EndFunc   ;==>_DataExchange_IsObject


; для перехвата ошибок WinHttpRequest

Func _DataExchange_SetErrorObj()

	Return ObjEvent("AutoIt.Error", "_DataExchange_CallbackCOM")

EndFunc   ;==>_DataExchange_SetErrorObj


; непосредственно перехват ошибок COM

Func _DataExchange_CallbackCOM()

EndFunc   ;==>_DataExchange_CallbackCOM


; основное назначение - убрать предупреждения при проверке кода

Func _DataExchange_ClearHandleCOM(ByRef $oErrorHandler)

	$oErrorHandler = 0

EndFunc   ;==>_DataExchange_ClearHandleCOM


Func DataExchange_SetJSON(ByRef $hDE, ByRef $sJSON)

	If Not _DataExchange_IsObject($hDE) Then Return

	$hDE[$eDE_vJSON] = $sJSON
	$sJSON = 0

EndFunc   ;==>DataExchange_SetJSON


; сделать массив поисковых систем, значениями в котором будут их базовые урлы;
; 2й и 3й параметры -- значения чекбоксов в интерфейсе (галка снята/проставлена)

Func DataExchange_SetEngines(ByRef $hDE, $iYandex, $iBing)

	If Not _DataExchange_IsObject($hDE) Then Return

	Local $sEngines = '' ; сначала в текст

	_DataExchange_AppendEngine($sEngines, $iYandex, "https://yandex.com")
	_DataExchange_AppendEngine($sEngines, $iBing, "https://www.bing.com")

	$hDE[$eDE_aEngines] = StringSplit($sEngines, @CRLF, $STR_ENTIRESPLIT)

EndFunc   ;==>DataExchange_SetEngines


; ф-ция помощник для корректной склейки строки с поисковиками

Func _DataExchange_AppendEngine(ByRef $sEngines, $iEngine, $sURL)

	If $iEngine Then

		If $sEngines Then $sEngines &= @CRLF

		$sEngines &= $sURL
	EndIf
EndFunc   ;==>_DataExchange_AppendEngine


Func DataExchange_Core(ByRef $hDE)

	If Not _DataExchange_IsObject($hDE) Then Return

	$hDE[$eDE_sPrettyPrint] = ''
	$hDE[$eDE_sMessageSent] = "Отправлено"
	$hDE[$eDE_sMessageError] = "Ошибка: "

	Local $vMessage

	For $i = 1 To ($hDE[$eDE_aEngines])[0]

		_DataExchange_SetEngine($hDE, ($hDE[$eDE_aEngines])[$i])

		$vMessage = _DataExchange_SendToEngine($hDE)

		_DataExchange_PrettifyResp($hDE, @error, $vMessage)
	Next
EndFunc   ;==>DataExchange_Core


Func _DataExchange_SetEngine(ByRef $hDE, $sEngineBase)

	Local $iEngine

	Select
		Case StringInStr($sEngineBase, "yandex.")

			$iEngine = $eDE_iYandex

		Case StringInStr($sEngineBase, "bing.")

			$iEngine = $eDE_iBing
	EndSelect

	$hDE[$eDE_iEngine] = $iEngine
	$hDE[$eDE_sEngineBase] = $sEngineBase

EndFunc   ;==>_DataExchange_SetEngine


; для отправки и получения данных с пом. WinHttpRequest

Func _DataExchange_SendToEngine(ByRef $hDE)

	Local $oErrorHandler = _DataExchange_SetErrorObj()

	Local $oHTTP = ObjCreate("WinHttp.WinHttpRequest.5.1")

	$oHTTP.Open("POST", $hDE[$eDE_sEngineBase] & "/indexnow", False)

	If @error Then Return SetError(1, @error, "Не получилось установить HTTP-соединение (код " & @error & ")")

	$oHTTP.SetRequestHeader("Content-Type", "application/json; charset=utf-8")

	$oHTTP.Send($hDE[$eDE_vJSON])

	If @error Then Return SetError(2, @error, "Обмен данными с сервером не удался (код " & @error & ")")

	$hDE[$eDE_iResponseCode] = $oHTTP.Status
	$hDE[$eDE_sTextResponse] = $oHTTP.ResponseText

	_DataExchange_ClearHandleCOM($oErrorHandler)

EndFunc   ;==>_DataExchange_SendToEngine


Func _DataExchange_GetPrettyEngine(ByRef Const $hDE)

	Local $sEngine

	Switch $hDE[$eDE_iEngine]

		Case $eDE_iYandex

			$sEngine = "Яндекс"

		Case $eDE_iBing

			$sEngine = "Bing"
	EndSwitch

	Return $sEngine

EndFunc   ;==>_DataExchange_GetPrettyEngine


; для сборки информации для пользователя

Func _DataExchange_PrettifyResp(ByRef $hDE, $iError, $vMessage)

	; если уже есть данные о каком-то поисковике, делаем отступ

	If $hDE[$eDE_sPrettyPrint] Then $hDE[$eDE_sPrettyPrint] &= @CRLF & @CRLF

	$hDE[$eDE_sPrettyPrint] &= _DataExchange_GetPrettyEngine($hDE)
	$hDE[$eDE_sPrettyPrint] &= @CRLF & "Код ответа: "

	If Not $iError Then
		$hDE[$eDE_sPrettyPrint] &= $hDE[$eDE_iResponseCode]
	Else
		$hDE[$eDE_sPrettyPrint] &= "-1"
	EndIf
	$hDE[$eDE_sPrettyPrint] &= @CRLF

	If $iError Then
		$hDE[$eDE_sPrettyPrint] &= $hDE[$eDE_sMessageError]
		$hDE[$eDE_sPrettyPrint] &= $vMessage
		Return
	EndIf

	$hDE[$eDE_vJSON] = Json_Decode($hDE[$eDE_sTextResponse])

	; у бинга по кривому сделано - не возвращается никакой JSON при успехе,
	; поэтому проверка только для Яндекса

	If $hDE[$eDE_iEngine] = $eDE_iYandex And Not IsObj($hDE[$eDE_vJSON]) Then

		$hDE[$eDE_sPrettyPrint] &= $hDE[$eDE_sMessageError]
		$hDE[$eDE_sPrettyPrint] &= "Сервер вернул некорректный JSON"
		Return
	EndIf

	$hDE[$eDE_sPrettyPrint] &= _DataExchange_GetPrettyStatus($hDE)

	; из-за той же кривизны бинга, когда при успехе вместо JSON возвращается пустота

	If $hDE[$eDE_iEngine] = $eDE_iBing And Not IsObj($hDE[$eDE_vJSON]) Then Return

	Local $sKey ; если ошибка, то 2 свойства в ответе: success и message (яндекс); code и message (бинг)

	Switch $hDE[$eDE_iEngine]

		Case $eDE_iYandex

			$sKey = "message"

		Case $eDE_iBing

			$sKey = "code"
	EndSwitch

	If $hDE[$eDE_vJSON].Exists($sKey) Then

		$hDE[$eDE_sPrettyPrint] &= $hDE[$eDE_vJSON].Item($sKey)
	EndIf
EndFunc   ;==>_DataExchange_PrettifyResp


Func _DataExchange_GetPrettyStatus(ByRef Const $hDE)

	Local $sStatus

	Switch $hDE[$eDE_iEngine]

		Case $eDE_iYandex

			$sStatus = ($hDE[$eDE_vJSON].Item("success") = "true") _
					 ? $hDE[$eDE_sMessageSent] : $hDE[$eDE_sMessageError]

		Case $eDE_iBing

			$sStatus = ($hDE[$eDE_iResponseCode] = 200 And Not IsObj($hDE[$eDE_vJSON])) _
					 ? $hDE[$eDE_sMessageSent] : $hDE[$eDE_sMessageError]
	EndSwitch

	Return $sStatus

EndFunc   ;==>_DataExchange_GetPrettyStatus

#EndRegion ; класс отправки и получения данных


#Region ; класс графического интерфейса

; для простого доступа к элементам интерфейса из любого "метода"
; (из ф-ции OnEvent, которая не может принимать параметры), иначе надо извращаться
; заданием текстовых идентификаторов элементов интерфейса и их последующей конвертацией в id

Global $g_hGUI

Global Const $eGUIGUID = "5ba10800-45c2-459e-bc0a-8e127c05cd6a"

Global Enum _
		$eGUI_sGUID, _                 ; GUID
		$eGUI_hWnd, _                  ; дескриптор главного окна
		$eGUI_hInpKey, _               ; поле ключа
		$eGUI_hChkNonStandard, _       ; чекбокс нестандартного ключа
		$eGUI_hInpNonStandard, _       ; поле нестандартного ключа
		$eGUI_hChkSaveKey, _           ; чекбокс сохранения ключа и путя к нестандартному (если есть)
		$eGUI_hChkYandex, _            ; чекбокс Яндекса
		$eGUI_hChkBing, _              ; чекбокс Bing'а
		$eGUI_hEdit, _                 ; поле урлов
		$eGUI_hDummySelectAll, _       ; поле фантомного элемента для ctrl+a
		$eGUI_hBtnClear, _             ; кнопка очистки поля урлов
		$eGUI_hBtnStart, _             ; кнопка старта
		$eGUI_hProgbar, _              ; полоса прогресс-бара
		$eGUI_iSize                    ; размер массива-"объекта"


Func GUI() ; конструктор

	Local $hWnd
	Local $hBtnStart
	Local $hEdit
	Local $hBtnClear
	Local $hDummySelectAll
	Local $hChkNonStandard
	Local $hInpNonStandard
	Local $hChkYandex
	Local $hChkBing
	Local $hChkSaveKey
	Local $hInpKey
	Local $hProgbar

	$hWnd = GUICreate($eMsgBoxTitle, 464, 489, -1, -1, -1, -1)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_GUI_Exit", $hWnd)
	$hInpKey = GUICtrlCreateInput("", 62, 16, 378, 24, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlCreateLabel("Ключ:", 21, 21, 37, 15, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "-2")
	$hProgbar = GUICtrlCreateProgress(0, 470, 468, 20, $PBS_MARQUEE, -1)
	GUICtrlSetState(-1, $GUI_SHOW)
	GUICtrlCreateLabel("Поисковые системы:", 27, 140, 135, 15, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "-2")
	GUICtrlCreateGroup("URL'ы", 25, 179, 414, 210, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$hChkNonStandard = GUICtrlCreateCheckbox("Нестандартный", 26, 59, 123, 20, -1, -1)
	GUICtrlSetOnEvent(-1, "_GUI_ToggleNonStandard")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Стандартный файл называется <ключ>.txt и находится в корневом каталоге")
	$hInpNonStandard = GUICtrlCreateInput("", 154, 58, 286, 24, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetState(-1, BitOR($GUI_SHOW, $GUI_DISABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Нестандартный путь к файлу ключа, например, http://example.com/catalog/key12457EDd.txt")
	$hChkSaveKey = GUICtrlCreateCheckbox("Сохранить ключ", 25, 97, 123, 20, -1, -1)
	GUICtrlSetState(-1, BitOR($GUI_CHECKED, $GUI_SHOW, $GUI_ENABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Ключ (и путь к нему, если указан) сохраняются в настройки (в шестнадцатеричном формате)")
	$hChkYandex = GUICtrlCreateCheckbox("Яндекс", 174, 139, 66, 20, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$hChkBing = GUICtrlCreateCheckbox("Bing", 254, 139, 47, 20, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$hEdit = GUICtrlCreateEdit("", 25, 217, 412, 173, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$hDummySelectAll = GUICtrlCreateDummy()
	GUICtrlSetOnEvent(-1, "_GUI_EditSelectAll")
	$hBtnClear = GUICtrlCreateButton("X", 403, 187, 36, 30, -1, -1)
	GUICtrlSetOnEvent(-1, "_GUI_EditClear")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Очистить")
	$hBtnStart = GUICtrlCreateButton("Отправить", 179, 409, 110, 48, -1, -1)
	GUICtrlSetOnEvent(-1, "_GUI_ExecOperations")
	GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")

	Local $hGUI[$eGUI_iSize]
	$hGUI[$eGUI_sGUID] = $eGUIGUID
	$hGUI[$eGUI_hWnd] = $hWnd
	$hGUI[$eGUI_hInpKey] = $hInpKey
	$hGUI[$eGUI_hChkNonStandard] = $hChkNonStandard
	$hGUI[$eGUI_hInpNonStandard] = $hInpNonStandard
	$hGUI[$eGUI_hChkSaveKey] = $hChkSaveKey
	$hGUI[$eGUI_hChkYandex] = $hChkYandex
	$hGUI[$eGUI_hChkBing] = $hChkBing
	$hGUI[$eGUI_hEdit] = $hEdit
	$hGUI[$eGUI_hDummySelectAll] = $hDummySelectAll
	$hGUI[$eGUI_hBtnClear] = $hBtnClear
	$hGUI[$eGUI_hBtnStart] = $hBtnStart
	$hGUI[$eGUI_hProgbar] = $hProgbar

	Return $hGUI

EndFunc   ;==>GUI


Func _GUI_IsObject(ByRef Const $hGUI)

	Return UBound($hGUI) = $eGUI_iSize And $hGUI[$eGUI_sGUID] == $eGUIGUID

EndFunc   ;==>_GUI_IsObject


Func GUI_Init()

	Local $hSettings = Settings()

	Settings_SetAll($hSettings)

	Local $hGUI = GUI()

	$g_hGUI = $hGUI

	GUI_Adapt($hGUI, $hSettings)

	GUISetState(@SW_SHOW, $hGUI[$eGUI_hWnd])

EndFunc   ;==>GUI_Init


; изменить изначальный вид интерфейса согласно настройкам и дополнить функционал

Func GUI_Adapt(ByRef Const $hGUI, ByRef Const $hSettings)

	If Not _GUI_IsObject($hGUI) Then Return

	GUICtrlSetState($hGUI[$eGUI_hProgbar], BitOR($GUI_HIDE, $GUI_DISABLE))

	GUICtrlSendMsg($hGUI[$eGUI_hEdit], $EM_LIMITTEXT, -1, 0) ; убираем ограничение на количество символов

	; для ctrl+a
	Local $aAccelKeys[1][2] = [["^a", $hGUI[$eGUI_hDummySelectAll]]]
	GUISetAccelerators($aAccelKeys, $hGUI[$eGUI_hWnd])

	If $hSettings[$eSett_sINKeyURL] Then ; когда задан путь к файлу ключа

		GUICtrlSetState($hGUI[$eGUI_hChkNonStandard], BitOR($GUI_CHECKED, $GUI_SHOW, $GUI_ENABLE))

		_GUI_ToggleNonStandard()

		GUICtrlSetData($hGUI[$eGUI_hInpNonStandard], $hSettings[$eSett_sINKeyURL])
	EndIf

	If Not $hSettings[$eSett_iSaveKey] Then _  ; если без сохранения ключа в настройки
			GUICtrlSetState($hGUI[$eGUI_hChkSaveKey], _
			BitOR($GUI_UNCHECKED, $GUI_SHOW, $GUI_ENABLE))

	If $hSettings[$eSett_sINKey] Then _ ; если в настройках есть ключ
			GUICtrlSetData($hGUI[$eGUI_hInpKey], $hSettings[$eSett_sINKey])

	If $hSettings[$eSett_iYandex] Then _
			GUICtrlSetState($hGUI[$eGUI_hChkYandex], BitOR($GUI_CHECKED, $GUI_SHOW, $GUI_ENABLE))

	If $hSettings[$eSett_iBing] Then _
			GUICtrlSetState($hGUI[$eGUI_hChkBing], BitOR($GUI_CHECKED, $GUI_SHOW, $GUI_ENABLE))

EndFunc   ;==>GUI_Adapt


; затемнение и его снятие для элементов

Func _GUI_ToggleControls(ByRef Const $hGUI)

	; если активно поле ключа, то затемняем всё

	If ControlCommand($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hInpKey], "IsEnabled") Then

		ControlDisable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hInpKey])
		ControlDisable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hChkNonStandard])
		; поле уже может быть выключено, но не проверяем:
		ControlDisable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hInpNonStandard])
		ControlDisable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hChkSaveKey])
		ControlDisable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hChkYandex])
		ControlDisable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hChkBing])
		ControlDisable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hEdit])
		ControlDisable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hBtnClear])
		ControlDisable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hBtnStart])
	Else
		ControlEnable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hInpKey])
		ControlEnable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hChkNonStandard])
		_GUI_ToggleNonStandard() ; нельзя просто включить, т.к. чекбокс может быть снят
		ControlEnable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hChkSaveKey])
		ControlEnable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hChkYandex])
		ControlEnable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hChkBing])
		ControlEnable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hEdit])
		ControlEnable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hBtnClear])
		ControlEnable($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hBtnStart])
	EndIf
EndFunc   ;==>_GUI_ToggleControls


Func _GUI_ToggleProgbar(ByRef Const $hGUI)

	If Not ControlCommand($hGUI[$eGUI_hWnd], '', $hGUI[$eGUI_hProgbar], "IsVisible") Then

		GUICtrlSetState($hGUI[$eGUI_hProgbar], BitOR($GUI_SHOW, $GUI_ENABLE))
		GUICtrlSendMsg($hGUI[$eGUI_hProgbar], $PBM_SETMARQUEE, 1, 50)
	Else
		GUICtrlSetState($hGUI[$eGUI_hProgbar], BitOR($GUI_HIDE, $GUI_DISABLE))
		GUICtrlSendMsg($hGUI[$eGUI_hProgbar], $PBM_SETMARQUEE, 0, 50)
	EndIf
EndFunc   ;==>_GUI_ToggleProgbar


; прерывание операций при ошибках для возврата к главному окну

Func _GUI_OnExecError(ByRef Const $hGUI, ByRef Const $vMessage, $iComboMB, $bToggleGUI = True)

	If $bToggleGUI Then

		_GUI_ToggleControls($hGUI)
		_GUI_ToggleProgbar($hGUI)
	EndIf

	MsgBox($iComboMB, $eMsgBoxTitle, $vMessage, 0, $hGUI[$eGUI_hWnd])

EndFunc   ;==>_GUI_OnExecError


; получить данные из поля edit

Func _GUI_GetURLs(ByRef Const $hGUI, ByRef $aURLs)

	Local $sList = GUICtrlRead($hGUI[$eGUI_hEdit])

	If Not $sList Or Not _StringNotEmpty($sList) Then _
			Return SetError(1, $MB_ICONWARNING, "Отсутствуют URL'ы")

	$aURLs = StringSplit($sList, @CRLF, $STR_ENTIRESPLIT + $STR_NOCOUNT)

	Local $iUbound = UBound($aURLs)

	For $i = $iUbound - 1 To 0 Step -1 ; идём снизу для захвата последних пустых строк

		If _StringNotEmpty($aURLs[$i]) Then ExitLoop ; на первой (снизу) непустой строке
	Next

	; если пустые строки были найдены, $i будет равен индексу последней непустой строки

	If Not ($i = $iUbound - 1) Then ReDim $aURLs[$i + 1]

EndFunc   ;==>_GUI_GetURLs


; получить данные из поля input

Func _GUI_GetInput(ByRef Const $hGUI, $i)

	Return GUICtrlRead($hGUI[$i])

EndFunc   ;==>_GUI_GetInput


Func _GUI_SaveSettings(ByRef Const $hGUI)

	Local $hSettings = Settings()

	Settings_CreateFile($hSettings)

	If @error Then Settings_SetAll($hSettings) ; файл существует, считать настройки

	Local $iSaveKey = _GUICtrlChkboxRadRead($hGUI[$eGUI_hChkSaveKey])

	Settings_WriteItem($hSettings, $eSett_sKeySaveKey, $iSaveKey)

	Settings_WriteItem($hSettings, $eSett_sKeyYandex, _
			_GUICtrlChkboxRadRead($hGUI[$eGUI_hChkYandex]))

	Settings_WriteItem($hSettings, $eSett_sKeyBing, _
			_GUICtrlChkboxRadRead($hGUI[$eGUI_hChkBing]))

	If $iSaveKey Then ; пишем если не снята галка

		Settings_WriteItem($hSettings, $eSett_sKeyINKey, _
				GUICtrlRead($hGUI[$eGUI_hInpKey]), True)

		Settings_WriteItem($hSettings, $eSett_sKeyINKeyURL, _
				GUICtrlRead($hGUI[$eGUI_hInpNonStandard]), True)
	Else
		; удаляем и ключ, и путь, если ранее были сохранены, но теперь галка снята

		Settings_WriteItem($hSettings, $eSett_sKeyINKey, '')
		Settings_WriteItem($hSettings, $eSett_sKeyINKeyURL, '')
	EndIf

	If Not _GUICtrlChkboxRadRead($hGUI[$eGUI_hChkNonStandard]) Then

		; когда снят чекбокс, но ранее было сохранено значение, тогда после перезапуска чекбокс
		; проставится вместе со значение, хотя если он был снят в момент выхода, значение более не нужно

		Settings_WriteItem($hSettings, $eSett_sKeyINKeyURL, '')
	EndIf
EndFunc   ;==>_GUI_SaveSettings


; ниже идут ф-ции OnEvent, используемые в интерфейсе


Func _GUI_Exit()

	Local $hGUI = $g_hGUI

	GUISetState(@SW_HIDE, $hGUI[$eGUI_hWnd])

	_GUI_SaveSettings($hGUI)

	Exit

EndFunc   ;==>_GUI_Exit


Func _GUI_ExecOperations()

	Local $hGUI = $g_hGUI

	_GUI_ToggleControls($hGUI)
	_GUI_ToggleProgbar($hGUI)

	Local $iYandex = _GUICtrlChkboxRadRead($hGUI[$eGUI_hChkYandex])
	Local $iBing = _GUICtrlChkboxRadRead($hGUI[$eGUI_hChkBing])

	If Not _GUI_GetInput($hGUI, $eGUI_hInpKey) Then _
			Return _GUI_OnExecError($hGUI, "Не задан ключ", $MB_ICONWARNING)

	If Not $iYandex And Not $iBing Then _
			Return _GUI_OnExecError($hGUI, "Не задано ни одной поисковой системы", $MB_ICONWARNING)

	Local $aURLs
	Local $vMessage = _GUI_GetURLs($hGUI, $aURLs)

	If @error Then Return _GUI_OnExecError($hGUI, $vMessage, @extended)

	Local $sKeyLoc = _ ; на случай, если вбито значение, но галка снята
			Not _GUICtrlChkboxRadRead($hGUI[$eGUI_hChkNonStandard]) _
			 ? '' : _GUI_GetInput($hGUI, $eGUI_hInpNonStandard)

	Local $sJSON
	Local $hPJ = PrepareJSON()
	PrepareJSON_Core($hPJ, $aURLs, _GUI_GetInput($hGUI, $eGUI_hInpKey), $sKeyLoc, $sJSON)
	$hPJ = 0

	Local $hDE = DataExchange()
	DataExchange_SetJSON($hDE, $sJSON)
	DataExchange_SetEngines($hDE, $iYandex, $iBing)
	DataExchange_Core($hDE)

	_GUI_ToggleControls($hGUI)
	_GUI_ToggleProgbar($hGUI)

	Return MsgBox($MB_ICONINFORMATION, $eMsgBoxTitle, _
			$hDE[$eDE_sPrettyPrint], 0, $hGUI[$eGUI_hWnd])

EndFunc   ;==>_GUI_ExecOperations


; включение/отключение поля нестандартного ключа только в зависимости от состояния управляющего чекбокса

Func _GUI_ToggleNonStandard()

	Local $hGUI = $g_hGUI

	If _GUICtrlChkboxRadRead($hGUI[$eGUI_hChkNonStandard]) Then

		GUICtrlSetState($hGUI[$eGUI_hInpNonStandard], BitOR($GUI_SHOW, $GUI_ENABLE))
	Else
		GUICtrlSetState($hGUI[$eGUI_hInpNonStandard], BitOR($GUI_SHOW, $GUI_DISABLE))
	EndIf
EndFunc   ;==>_GUI_ToggleNonStandard


Func _GUI_EditClear()

	Local $hGUI = $g_hGUI

	GUICtrlSetData($hGUI[$eGUI_hEdit], '')

EndFunc   ;==>_GUI_EditClear


; will make select all text in eny focused edit, as long as it is edit inside
; $hWnd winhandle because we bound GUISetAccelerators to $hWnd

Func _GUI_EditSelectAll()

	Local $hWnd = _WinAPI_GetFocus() ; с дескриптором интерфейса из "объекта" почему-то не работает

	If _WinAPI_GetClassName($hWnd) = "Edit" Then _GUICtrlEdit_SetSel($hWnd, 0, -1)

EndFunc   ;==>_GUI_EditSelectAll

#EndRegion ; класс графического интерфейса


#Region ; выполнение

GUI_Init()

While 1 ; простой GUI

	Sleep(100)
WEnd

#EndRegion ; выполнение
