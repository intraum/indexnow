## IndexNow - массовая отправка URL'ов
Десктопная утилита для Винды для массовой отправки URL'ов по "протоколу" IndexNow.  
Описание [здесь](https://seopart.ru/software/indexnow).  
Общее описание [здесь](desc_common.md).  
### Используемые компоненты
[JSON UDF](https://www.autoitscript.com/forum/topic/148114-a-non-strict-json-udf-jsmn/) - для конвертации данных в JSON и обратно.
